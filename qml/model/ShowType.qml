import QtQuick 2.12

QtObject {
    property string uuid: ""
    property string image: ""
    property string name: ""
    property string owner: ""
    property int followers: 0
    property int size: 0
    property string follow: ""

    function getTrack(index) {
        return spotSession.getTrackByShow(uuid, index);
    }
}