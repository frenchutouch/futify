import QtQuick 2.12
import QtQuick.Controls 2.12
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

import "../model"

ListView {
    property AlbumType album
    
    signal startAlbum(AlbumType album)
    signal startTrack(var track)
    signal addToEndQueue(var track)
    signal follow(AlbumType album)
    signal unfollow(AlbumType album)

    clip: true

    header: ListItem {
        height: units.gu(20)
        width: parent.width
        
        Row {
            height: parent.height
            width: parent.width
            padding: units.gu(2)
            spacing: units.gu(2)

            Item {
                id: itemImage
                height: units.gu(16)
                width: itemImage.height
                anchors.verticalCenter: parent.verticalCenter
                Image {
                    id: image
                    source: album.image
                    width: parent.width
                    height: parent.height
                    anchors.verticalCenter: parent.verticalCenter
                    fillMode: Image.PreserveAspectFit
                    visible: false
                }
                OpacityMask {
                    anchors.fill: image
                    source: image
                    width: image.width
                    height: image.height
                    maskSource: Rectangle {
                        width: image.width
                        height: image.height
                        radius: 5
                        visible: false // this also needs to be invisible or it will cover up the image
                    }
                }
                // PLAY
                Rectangle {
                    color: "#99FFFFFF"
                    width: units.gu(5)
                    height: units.gu(5)
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    radius: 50
                }
                Item {
                    width: parent.width
                    height: parent.height
                    anchors.top: parent.top
                    TapHandler {
                        onTapped: startAlbum(album)
                    }
                }
                Icon {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    name: 'media-preview-start'
                    color: "#000000"
                    width: units.gu(5)
                    height: units.gu(5)
                }
            }
            Column {
                anchors.verticalCenter: itemImage.verticalCenter
                width: parent.width - itemImage.width
                height: text1.height + text2.height + btnFollow.height
                Label { id: text1; text: album.name; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount: 1; textSize: Label.Large }
                Label { id: text2; text: album.group; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount:1; }
                Button {
                    id: btnFollow
                    visible: album.follow !== "followImpossible"
                    text: album.follow === "follow" ? qsTr("Remove from my library") : qsTr("Add to my library")
                    onClicked: {
                        album.follow === "follow" ? unfollow(album) : follow(album)
                    }
                }
            }
        }
    }

    model: album.size

    ScrollBar.vertical: ScrollBar {
        active: true
    }

    delegate: TrackListItem {
        id: trackListItem
        track: album.getTrack(index)
        canDelete: false
        activeSimpleClick: true
        onPlayTrack: {
            console.log('Album.qml => start song', trackListItem.track.name)
            startTrack(track);
        }
        onEndQueue: {
            console.log('Album.qml => end queue song', trackListItem.track.name)
            addToEndQueue(track);
        }
    }
}