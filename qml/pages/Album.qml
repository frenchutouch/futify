import QtQuick 2.12
import Lomiri.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property AlbumType album

    signal startAlbum(AlbumType album)
    signal startTrack(var track)
    signal addToEndQueue(var track)
    signal follow(AlbumType playlist)
    signal unfollow(AlbumType playlist)

    function refresh(albumTypeFactory) {
        if (album.uuid != '') {
            album = albumTypeFactory.createObject(page, spotSession.getAlbum(spotSession.loadAlbum(album.uuid)));
        }
    }

    header: PageHeader {
        id: header
        title: album.name
        StyleHints {
            dividerColor: LomiriColors.green
        }
    }

    AlbumView {
        album: page.album
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onStartAlbum: page.startAlbum(album)
        onStartTrack: page.startTrack(track)
        onAddToEndQueue: page.addToEndQueue(track)
        onFollow: page.follow(album)
        onUnfollow: page.unfollow(album)
    }
}